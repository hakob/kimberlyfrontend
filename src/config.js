import React from 'react';
import Select from 'react-select';

import { Redirect } from 'react-router';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'





const stateOptions = [
    {value: '1', label: 'US Wide'},
    {value: '2',isDisabled:true, label: 'US Narrow'},
    {value: '3', isDisabled:true,label: 'Europe'}
];


const TXOptions = [
        {value: 13, label: '13'},
        {value: 14, label: '14'},
        {value: 15, label: '15'},
        {value: 16, label: '16'},
        {value: 17, label: '17'},
        {value: 18, label: '18'},
        {value: 19, label: '19'},
        {value: 20, label: '20'},
        {value: 21, label: '21'},
        {value: 22, label: '22'},
        {value: 23, label: '23'},
        {value: 24, label: '24'},
        {value: 25, label: '25'},
        {value: 26, label: '26'},
        {value: 27, label: '27'},

    ];
const FROptions = [
    {value: 0, label: 'Frequency hopping only'},
    {value: 1, label: 'Listen before talk only'},
    {value: 2, label: 'Frequency hopping with listen before talk feature'},
    {value: 3, label: 'Listen before talk with frequency hopping feature'},
    {value: 4, label: 'FH and LBT disabled'},

];

const FHOptions = [
    {value: '1', label: '902.75 MHz'},
    {value: '2', label: '903.25 MHz'},
    {value: '3', label: '903.75 MHz'},
    {value: '4', label: '904.25 MHz'},
    {value: '5', label: '904.75 MHz'},
    {value: '6', label: '905.25 MHz'},
    {value: '7', label: '905.75 MHz'},
    {value: '8', label: '905.75 MHz'},
    {value: '9', label: '906.75 MHz'},
    {value: '10', label: '907.25 MHz'},
    {value: '11', label: '907.75 MHz'},
    {value: '12', label: '908.25 MHz'},
    {value: '13', label: '908.75 MHz'},
    {value: '14', label: '909.25 MHz'},
    {value: '15', label: '909.25 MHz'},
    {value: '16', label: '910.25 MHz'},
    {value: '17', label: '910.75 MHz'},
    {value: '18', label: '911.25 MHz'},
    {value: '19', label: '911.75 MHz'},
    {value: '20', label: '912.25 MHz'},
    {value: '21', label: '912.75 MHz'},
    {value: '22', label: '913.25 MHz'},
    {value: '23', label: '913.75 MHz'},
    {value: '24', label: '914.25 MHz'},
    {value: '25', label: '914.75 MHz'},
    {value: '26', label: '915.25 MHz'},
    {value: '27', label: '915.75 MHz'},
    {value: '28', label: '916.25 MHz'},
    {value: '29', label: '916.75 MHz'},
    {value: '30', label: '917.25 MHz'},
    {value: '31', label: '917.75 MHz'},
    {value: '32', label: '918.25 MHz'},
    {value: '33', label: '918.75 MHz'},
    {value: '34', label: '919.25 MHz'},
    {value: '35', label: '919.75 MHz'},
    {value: '36', label: '920.25 MHz'},
    {value: '37', label: '920.75 MHz'},
    {value: '38', label: '921.25 MHz'},
    {value: '39', label: '921.75 MHz'},
    {value: '40', label: '922.25 MHz'},
    {value: '41', label: '922.75 MHz'},
    {value: '42', label: '923.25 MHz'},
    {value: '43', label: '923.75 MHz'},
    {value: '44', label: '924.25 MHz'},
    {value: '45', label: '924.75 MHz'},
    {value: '46', label: '925.25 MHz'},
    {value: '47', label: '925.75 MHz'},
    {value: '48', label: '926.25 MHz'},
    {value: '49', label: '926.75 MHz'},
    {value: '50', label: '927.25 MHz'},


];
const numberOptions = [
    {value: '903.25 MHz', label: '1'},
    {value: '903.75 MHz', label: '2'},
    {value: '904.25 MHz', label: '3'},
    {value: '904.75 MHz', label: '4'},
    {value: '905.25 MHz', label: '5'},
    {value: '905.75 MHz', label: '6'},
    {value: '905.75 MHz', label: '7'},
    {value: '906.75 MHz', label: '8'},
    {value: '907.25 MHz', label: '9'},
    {value: '907.75 MHz', label: '10'},
    {value: '908.25 MHz', label: '11'},
    {value: '908.75 MHz', label: '12'},
    {value: '909.25 MHz', label: '14'},
    {value: '909.25 MHz', label: '15'},
    {value: '910.25 MHz', label: '16'},
    {value: '910.75 MHz', label: '17'},
    {value: '911.25 MHz', label: '18'},
    {value: '911.75 MHz', label: '19'},
    {value: '912.25 MHz', label: '20'},
    {value: '912.75 MHz', label: '21'},
    {value: '913.25 MHz', label: '22'},
    {value: '913.75 MHz', label: '23'},
    {value: '914.25 MHz', label: '24'},
    {value: '914.75 MHz', label: '25'},
    {value: '915.25 MHz', label: '26'},
    {value: '915.75 MHz', label: '27'},
    {value: '916.25 MHz', label: '28'},
    {value: '916.75 MHz', label: '29'},
    {value: '917.25 MHz', label: '30'},
    {value: '917.75 MHz', label: '31'},
    {value: '918.25 MHz', label: '32'},
    {value: '918.75 MHz', label: '33'},
    {value: '919.25 MHz', label: '34'},
    {value: '919.75 MHz', label: '35'},
    {value: '920.25 MHz', label: '36'},
    {value: '920.75 MHz', label: '37'},
    {value: '921.25 MHz', label: '38'},
    {value: '921.75 MHz', label: '39'},
    {value: '922.25 MHz', label: '40'},
    {value: '922.75 MHz', label: '41'},
    {value: '923.25 MHz', label: '42'},
    {value: '923.75 MHz', label: '43'},
    {value: '924.25 MHz', label: '44'},
    {value: '924.75 MHz', label: '45'},
    {value: '925.25 MHz', label: '46'},
    {value: '925.75 MHz', label: '47'},
    {value: '925.75 MHz', label: '48'},
    {value: '926.75 MHz', label: '49'},
    {value: '927.25 MHz', label: '50'},

];

class Config extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            source:undefined,
            region: "1",
            interval:60,
            txpower: undefined,
            lbtMode: undefined,
            fhtable: [25, 18, 23, 34, 49, 3, 13, 26, 38, 47, 1, 16, 30, 40,
                45, 10, 14, 28, 36, 43, 4, 17, 6, 32, 46, 7, 12, 27,
                37, 42,  9, 19, 22, 31, 48, 2, 11, 29, 39, 41, 5, 20,
                21, 35, 50, 8, 15, 24, 33, 44 ],
            readtime: 380,
            idleTime: 100,
            carTime: 10,
            targetLevel: -74,
            submitted: false,
            opened: false,
            hasError: false,
            ithasError: false,
            fireRedirect: false


        };

        this.isValidationError = this.isValidationError.bind(this);
        this.closeBox = this.toggleBox.bind(this);
        this.toggleBox = this.toggleBox.bind(this);
        this.handlelevelChange = this.handlelevelChange.bind(this);
        this.handlereadTimeChange = this.handlereadTimeChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handlecarTimeChange = this.handlecarTimeChange.bind(this);
        this.handleinervalChange= this.handleinervalChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (value, state) => {
        this.setState({[state]: value.value});
    };
    handlefChange = (value, state) => {

        const array = [];
        value.forEach(i => array.push(i.value))
        this.setState({[state]: array})

    };

    handlelevelChange(e) {
        this.setState({targetLevel: e.target.value});
    }

    handlereadTimeChange(e) {
        this.setState({readtime: e.target.value});
    }


    handleTimeChange(e) {
        this.setState({idleTime: e.target.value});
    }

    handlecarTimeChange(e) {
        this.setState({carTime: e.target.value});
    }
    handleinervalChange(e) {
        this.setState({interval: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({submitted: true});

        const body = {

            region: this.state.region,
            publish_intvl:this.state.interval,
            tx_pwr: this.state.txpower,
            dense_mode: this.state.lbtMode,
            fh_table: this.state.fhtable,
            cs_time: this.state.carTime,
            read_time: this.state.readtime,
            idle_time: this.state.idleTime,
            target_pwr: this.state.targetLevel*10
        }


        let isValid =true
        if (body.tx_pwr === undefined) {
            isValid=false
            this.setState({hasError: true})
            isValid=false
        }
        if (body.dense_mode === undefined) {
            isValid=false
            this.setState({ithasError: true})

        }



         if(isValid) {

                confirmAlert({
                    title: ' Success',
                    message: 'Press OK to continue',

                    buttons: [
                        {
                            label: 'OK',
                            onClick: () => this.setState({ fireRedirect: true})
                        },


                    ]
                })

           // this.setState({ fireRedirect: true})
            fetch('/api/config', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json, text/plain, ',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }).then(res => res)
                .then(res => res.status === "success" ? alert('Config  done') : console.log(res));
        }






    }

    toggleBox() {
        this.setState(function (prevState) {
            return {opened: !prevState.opened};
        });

    }

    isValidationError(flag) {
        this.isFormValidationErrors = flag;
    }


    render() {
        const {opened} = this.state;
        console.log(this.state.fhtable)

        return (

            <div className="container l" id="responsive">


                <div className="topcorner">
                    <h7 className="mb-3">Advanced settings <button onClick={this.toggleBox}>
                        {this.state.opened ? 'ON' : 'OFF'}
                    </button></h7>
                </div>

                <form className='form'>

                    <fieldset>
                        <div className="form-group">
                            <label htmlFor="Region">Region</label>
                            <Select options={stateOptions}
                                    onChange={(value) => this.handleChange(value, "region")}
                                    placeholder={'US Wide'}
                                    requeried={true}
                                    />



                        </div>
                        <div className="form-group">
                            <label htmlFor="TX Power Level">TX Power Level [dBm]</label>
                            <Select options={TXOptions}
                                    onChange={(value) => this.handleChange(value, "txpower")}

                            />
                            {this.state.hasError === true && (
                                <span className="text-danger">Required</span>
                            )}

                        </div>
                        <div className="form-group">
                            <label htmlFor="FH and LBT modeFH and LBT mode">FH and LBT mode</label>

                            <Select options={FROptions}
                                    onChange={(value) => this.handleChange(value, "lbtMode")}/>
                            {this.state.ithasError === true && (
                                <span className="text-danger">Required</span>
                            )}

                        </div>

                        {opened && (<div className="container ">
                                <div className="form-group">

                                    <div className="row">


                                        <div className="col">

                                            <label htmlFor="FH table">Publish Interval [sec]</label>
                                            <input type="text" id="disabledTextInput" className="form-control"
                                                   placeholder="Time Interval"
                                                   value={this.state.interval}
                                                   onChange={this.handleinervalChange}/></div>

                                        <div className="col">
                                            <label htmlFor="FH table">FH table</label>
                                            <Select
                                                isMulti
                                                onChange={(value) => this.handlefChange(value, "fhtable")}
                                                options={FHOptions}
                                            /></div>
                                    </div>

                                </div>
                                <div className="form-group">
                                    <label htmlFor="Read Time">Read Time [ms]</label>
                                    <input type="text" id="disabledTextInput" className="form-control"
                                           placeholder="Read Time"
                                           value={this.state.readtime}
                                           onChange={this.handlereadTimeChange}/>
                                </div>


                                <div className="form-group">
                                    <label htmlFor="Idle Time">Idle Time [ms]</label>
                                    <input type="text" id="disabledTextInput" className="form-control"
                                           placeholder="Idle Time"
                                           value={this.state.idleTime}
                                           onChange={this.handleTimeChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="Carrier Sense Time ">Carrier Sense Time [ms]</label>
                                    <input type="text" id="disabledTextInput" className="form-control"
                                           placeholder="Carrier Sense Time"
                                           value={this.state.carTime}
                                           onChange={this.handlecarTimeChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="Target RF Power Level">Target RF Power Level [dBm]</label>
                                    <input type="text" id="disabledTextInput" className="form-control"
                                           placeholder="Target RF Power Level"
                                           value={this.state.targetLevel}
                                           onChange={this.handlelevelChange}/>
                                </div>
                            </div>


                        )}


                        <button type="" className="btn btn-outline-success my-2 my-sm-0"
                                onClick={this.handleSubmit}>Submit

                        </button>
                         { this.state.fireRedirect && (
                            <Redirect to={ '/home'}/>
                        )}
                    </fieldset>
                </form>

            </div>

        )
    }
}


export default Config;
