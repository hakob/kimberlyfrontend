import React, {Component} from 'react';
//import { Router,browserHistory  } from 'react-router'
import Main from './routes'
import './App.css';
import Navigation from "./Navigation";
import Footer from "./Footer";
import Modal from 'react-awesome-modal';
import FineUploaderTraditional from 'fine-uploader-wrappers';
import Gallery from 'react-fine-uploader';
import 'react-fine-uploader/gallery/gallery.css'


const uploader = new FineUploaderTraditional({
    options: {
        chunking: {
            enabled: true
        },

        deleteFile: {
            enabled: true,
            endpoint: 'patch'
        },
        request: {
            endpoint: 'patch'
        },
        retry: {
            enableAuto: true

        },
        callbacks: {
            onComplete: function (id, fileName, response, xhr) {

                if(response.succes=true){
                   const body ={
                        source :response.url
                    };

                    fetch('/api/update', {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json, text/plain, ',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(body)
                    }).then(res => res)
                        .then(res => console.log(res));
                }

            },
            onUploadChunkSuccess: function (id, chunkData, response, xhr) {
                console.log(response);
            }

        }
    }
})

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {


            visible: false,

        }

        this.openModal = this.openModal.bind(this)


    }


    openModal() {
        this.setState({
            visible: true
        });
    }

    closeModal() {
        this.setState({
            visible: false,

        });
    }

    render() {
        return (
            <div className="App">
                <Navigation passedFunction={this.openModal}/>

                <Main/>


                <Modal visible={this.state.visible} width="500" height="300" effect="fadeInUp"
                       onClickAway={() => this.closeModal()}>
                    <Gallery uploader={uploader}/>
                </Modal>

            </div>


        );
    }
}

export default App;
