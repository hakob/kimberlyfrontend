import React from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from './Home';
import Config from "./config";


const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/home' component={Home}/>
            <Route path='/config' component={Config}/>
        </Switch>
    </main>
);

export default Main
