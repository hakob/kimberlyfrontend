import React, {Component} from 'react';
import './App.css';
import Modal from 'react-awesome-modal';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            hit: [],
            id: '',
            tags: []
        };

    }

    componentDidMount() {
        fetch('/api/home')
            .then((res) => res.json())
            .then((res) => {
                    const hits = [];

                    res.forEach(item => {
                        hits.push(item.key)
                    });
                    // console.log(hits)
                    if (hits.length > 0) this.setState({hit: this.state.hit.concat(hits)})
                    else this.setState({hit: this.state.hit})

                }
            );
    }

    openModal(id) {
        fetch('api/tags', {
            method: 'POST', // or 'PUT'
            body: JSON.stringify({id}), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => this.setState({tags: this.state.tags.concat(response)}))
            .catch(error => console.error('Error:', error));


        this.setState({
            visible: true,
            id: id
        });
    };

    closeModal() {
        this.setState({
            visible: false,
            tags: []
        });
    }


    render() {
        let self = this
        const data = this.state.hit
        const tagData = this.state.tags
        return (
            <div className="list-group ">

                { data.length > 0 ?
                <table id="tmytable" className="table table-striped">

                    <thead>
                    <th>Device</th>
                    <th>Region</th>
                    <th>ID</th>
                    <th>Tags</th>


                    </thead>
                    <tbody>
                    {




                       (data.map(function (d, idx) {

                                return (
                                    <tr>
                                        <td>RFID Reader</td>
                                        <td>US Wide</td>
                                        <td>{d}</td>
                                        <td>

                                           <button
                                            className="btn btn-info" onClick={() => self.openModal(d)}>
                                               Info
                                           </button>

                                        </td>


                                    </tr>

                                )
                            }))



                                }




                    <section>


                        <Modal visible={this.state.visible} width="600" height="600" effect="fadeInUp"
                               onClickAway={() => this.closeModal()}>

                            <div className="table-wrapper-scroll-y">



                                { tagData.length > 0 ? <table  className=" table table-fixed table-striped">

                                    <thead className="thead-dark">
                                    <th>Time</th>
                                    <th>ID</th>
                                    <th>RSSI</th>


                                    </thead>
                                    <tbody>


                                    {(tagData.map(function (d) {
                                        return (
                                            <tr>
                                                <td >{new Date(d.time).toLocaleString()}</td>
                                                <td >{d.id}</td>
                                                <td > {d.rssi} dBm</td>


                                            </tr>

                                        )


                                    }))}


                                    </tbody>

                                </table>

                                    :
                                    <div className="container">
                                        <div id="black"><h4>Tag Information</h4></div>

                                    <div className="container" id="new">


                                            <div className="alert alert-info" role="alert" id='warn'>
                                                <strong>No Available Tags! </strong>
                                            </div>

                                    </div>
                                    </div>
                                    }


                                {/*  <button className="btn-primary "  type="submit" onClick={() => this.closeModal()}> Close</button>*/}
                            </div>

                        </Modal>
                    </section>




                    </tbody>

                </table>
                   :



                    <div className="container" id="new">

                        <div className="alert alert-info" role="alert" id='warn'>
                            <strong>No Available Devices! </strong>
                        </div>
                    </div>


                    }

            </div>
        );
    }
}


export default Home;
